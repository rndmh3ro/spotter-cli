Go through the checkboxes below and tick off your assigned ones, _in sequence_.
If a bug is found and a previous step is restarted, all subsequent checkboxes must be done again.

Example: a bug is found when verifying the _very last_ feature of the release candidate.
A bugfix is created, a new RC tag is created, the tag is logged at the top of the checklist as a
new checkmark. _All other_ features must also be verified again with the latest RC release.

* [ ] Tag release candidate version X.Y.Zrc1. @matejart
* [ ] Verify the features work correctly on the release candidate:
	* [ ] Feature 1 expectation ... @ responsible
* [ ] Approve that no changes need to be added to this release @gregor.berginc
* [ ] Changelog is up to date. @matejart
* [ ] Assign production version to the commit previously tagged as RC. @matejart

/label ~"Release checklist"
/cc @gregor.berginc
/assign @matejart
