# syntax=docker/dockerfile:1
ARG BASE_IMAGE_PREFIX=""
FROM ${BASE_IMAGE_PREFIX}library/debian:bullseye-20220228-slim
ARG PYENV_VERSION="2.3.13"
ARG PYENV_VIRTUALENV_VERSION="1.2.1"

ARG BUILD_ENV="\
        py3.11.1-ansi2.14.2  \
        py3.11.1-ansi2.13.7  \
        py3.11.1-ansi2.12.10 \
        py3.11.1-ansi2.11.12 \
        py3.11.1-ansi2.10.17 \
        py3.11.1-ansi2.9.27  \
        py3.10.9-ansi2.14.2  \
        py3.10.9-ansi2.13.7  \
        py3.10.9-ansi2.12.10 \
        py3.10.9-ansi2.11.12 \
        py3.10.9-ansi2.10.17 \
        py3.10.9-ansi2.9.27  \
        py3.9.16-ansi2.14.2  \
        py3.9.16-ansi2.13.7  \
        py3.9.16-ansi2.12.10 \
        py3.9.16-ansi2.11.12 \
        py3.9.16-ansi2.10.17 \
        py3.9.16-ansi2.9.27  \
        py3.8.16-ansi2.13.7  \
        py3.8.16-ansi2.12.10 \
        py3.8.16-ansi2.11.12 \
        py3.8.16-ansi2.10.17 \
        py3.8.16-ansi2.9.27  \
        py3.7.16-ansi2.11.12 \
        py3.7.16-ansi2.10.17 \
        py3.7.16-ansi2.9.27  \
"

SHELL ["/bin/bash", "-c"]
ENTRYPOINT ["/entrypoint.sh"]

ENV PYENV_ROOT "/opt/pyenv-${PYENV_VERSION}"
ENV PATH "$PYENV_ROOT/bin:$PATH"


# Install required packages
# hadolint ignore=DL4006,DL3042
RUN <<EOF
set -euxo pipefail

apt-get update -y
# Install base and pyenv dependencies
apt-get install -y --no-install-recommends \
            wget=1.21-1+deb11u1 \
            unzip=6.0-26+deb11u1 \
            git=1:2.30.2-1+deb11u2 \
            build-essential=12.9 \
            libssl-dev=1.1.1n-0+deb11u5 \
            zlib1g-dev=1:1.2.11.dfsg-2+deb11u2 \
            libbz2-dev=1.0.8-4 \
            libreadline-dev=8.1-1 \
            libsqlite3-dev=3.34.1-3 \
            curl=7.74.0-1.3+deb11u7 \
            libncursesw5-dev=6.2+20201114-2+deb11u1 \
            xz-utils=5.2.5-2.1~deb11u1 \
            tk-dev=8.6.11+1 \
            libxml2-dev=2.9.10+dfsg-6.7+deb11u4 \
            libxmlsec1-dev=1.2.31-1 \
            libffi-dev=3.3-6 \
            liblzma-dev=5.2.5-2.1~deb11u1 \
            ca-certificates=20210119

# Download and install pyenv
wget -nv "https://github.com/pyenv/pyenv/archive/refs/tags/v${PYENV_VERSION}.zip" -O /tmp/pyenv.zip
unzip -q /tmp/pyenv.zip -d /opt

# Download and install pyenv virtualenv
wget -nv "https://github.com/pyenv/pyenv-virtualenv/archive/refs/tags/v${PYENV_VIRTUALENV_VERSION}.zip" -O /tmp/pyenv-venv.zip
unzip -q /tmp/pyenv-venv.zip -d "/tmp/tmp_venv"
cp -r "/tmp/tmp_venv/pyenv-virtualenv-${PYENV_VIRTUALENV_VERSION}" "${PYENV_ROOT}/plugins/pyenv-virtualenv"

# Add pyenv to PATH and initialize
echo "export PATH=""${PYENV_ROOT}"/bin:"${PATH}""" >> /root/.bashrc
echo eval "$(pyenv init -)" >> /root/.bashrc
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Create a temporary directory for pip cache
mkdir /tmp/pip_cache_dir

for env in $(echo "$BUILD_ENV" | tr " " " "); do
    python_v=$(echo "$env" | grep -oE "py([0-9]+\.[0-9]+\.[0-9]+)" | grep -oE "([0-9]+\.[0-9]+\.[0-9]+)")
    ansi_v=$(echo "$env" | grep -oE "ansi([0-9]+\.[0-9]+\.[0-9]+)" | grep -oE "([0-9]+\.[0-9]+\.[0-9]+)")
    major_ansi_v=$(echo "$ansi_v" | cut -d '.' -f2)

    if [ -z "$python_v" ]; then
        echo "Error: Invalid python version provided !"
        exit 2
    fi

    if [ -z "$ansi_v" ]; then
        echo "Error: Invalid ansible version provided!"
        exit 3
    fi

    # Check if wanted python version is installed
    if ! pyenv versions | grep "$python_v" > /dev/null; then
        pyenv install "${python_v}"
    fi

    pyenv virtualenv "${python_v}" "py${python_v}-ansi${ansi_v}"
    pyenv activate "py${python_v}-ansi${ansi_v}"

    # Install proper ansible
    if [ "${major_ansi_v}" -lt "10" ]; then
        pip install --cache-dir=/tmp/pip_cache_dir ansible=="${ansi_v}"
    elif [ "${major_ansi_v}" -eq "10" ]; then
        pip install --cache-dir=/tmp/pip_cache_dir ansible-base=="${ansi_v}"
    else
        pip install --cache-dir=/tmp/pip_cache_dir ansible-core=="${ansi_v}"
    fi

    pyenv deactivate
done

# Cleanup
find / -name __pycache__ -print0 | xargs -0 rm -rf \
    && apt-get autoremove -y \
    && apt-get autoclean -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/* \
    && rm -rf /root/.cache/ \
    && rm -rf /tmp/*

EOF

COPY entrypoint.sh /entrypoint.sh
