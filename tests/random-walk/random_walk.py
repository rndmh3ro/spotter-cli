import argparse
import os
import random
import subprocess
import sys
from argparse import ArgumentDefaultsHelpFormatter
from pathlib import Path
from typing import Any, Optional

import requests


class RandomWalk:
    # pylint: disable=too-many-instance-attributes
    DEFAULT_TIMEOUT = 5
    DEFAULT_HEADERS = {
        "Accept": "application/json",
        "User-Agent": "steampunk-spotter/RandomWalk"
    }

    def __init__(self, args: argparse.Namespace):
        self.username1: str = args.username1
        self.password1: str = args.password1
        self.api_token1: str = args.api_token1
        self.endpoint1: str = args.endpoint1

        self.username2: str = args.username2
        self.password2: str = args.password2
        self.api_token2: str = args.api_token2
        self.endpoint2: str = args.endpoint2

        self.api_token1_b_json: Any = {}
        self.api_token2_b_json: Any = {}

        self.scan_input: str = args.scan_input

        self.storage_path = Path.home() / ".config/steampunk-spotter"
        self.old_tokens_file = self.storage_path / "tokens"
        self.tokens_file = self.storage_path / "tokens.json"

    def get_user_id(self, endpoint: str, api_token: str, timeout: int = DEFAULT_TIMEOUT) -> str:
        headers = self.DEFAULT_HEADERS
        headers["Authorization"] = f"SPTKN {api_token}"

        response = requests.get(
            endpoint + "/v2/users/me/",
            headers=headers,
            timeout=timeout
        )
        if response.status_code != 200:
            print(f"Error {response.status_code} getting user id: {response.json()}", file=sys.stderr)
            sys.exit(1)

        id: str = response.json()["id"]
        return id

    def create_api_token_json(self, endpoint: str, api_token: str, timeout: int = DEFAULT_TIMEOUT) -> Any:
        headers = self.DEFAULT_HEADERS
        headers["Authorization"] = f"SPTKN {api_token}"

        user_id = self.get_user_id(endpoint, api_token, timeout)

        response = requests.post(
            endpoint + f"/v2/users/{user_id}/api_tokens/",
            headers=headers,
            json={"name": "RandomWalk's API key B"},
            timeout=timeout
        )
        if response.status_code != 201:
            print(
                f"Error  {response.status_code} creating API token: {response.json()}",
                file=sys.stderr
            )
            sys.exit(1)

        return response.json()

    def delete_api_token(self, endpoint: str, api_token: str, id_token_to_delete: str,
                         timeout: int = DEFAULT_TIMEOUT) -> None:
        headers = self.DEFAULT_HEADERS
        headers["Authorization"] = f"SPTKN {api_token}"

        user_id = self.get_user_id(endpoint, api_token, timeout)

        response = requests.delete(
            endpoint + f"/v2/users/{user_id}/api_tokens/{id_token_to_delete}",
            headers=headers,
            timeout=timeout
        )
        if response.status_code != 204:
            print(
                f"Error response {response.status_code} deleting API token",
                file=sys.stderr
            )
            sys.exit(1)

    def _get_global_switches(self, endpoint: str, username: Optional[str] = None,
                             password: Optional[str] = None,
                             api_token: Optional[str] = None) -> list[str]:
        global_switches: list[str] = ["--endpoint", endpoint]
        if username is not None:
            global_switches.extend(["--username", username])
        if password is not None:
            global_switches.extend(["--password", password])
        if api_token is not None:
            global_switches.extend(["--api-token", api_token])
        return global_switches

    def _run(self, cmds: list[str]) -> tuple[str, str]:
        print(f"Running: {' '.join(cmds)}")
        result = subprocess.run(cmds, capture_output=True, check=False)
        return result.stdout.decode('utf-8'), result.stderr.decode('utf-8')

    def run_scan(self, endpoint: str, scan_input: str, username: Optional[str] = None,
                 password: Optional[str] = None, api_token: Optional[str] = None) -> tuple[str, str]:
        cmds: list[str] = ["spotter"]
        cmds.extend(self._get_global_switches(endpoint, username, password, api_token))
        cmds.extend(["scan", scan_input])

        return self._run(cmds)

    def login(self, endpoint: str, username: Optional[str] = None, password: Optional[str] = None,
              api_token: Optional[str] = None) -> tuple[str, str]:
        cmds: list[str] = ["spotter"]
        cmds.extend(self._get_global_switches(endpoint, username, password, api_token))
        cmds.extend(["login"])
        print(cmds)

        return self._run(cmds)

    def delete_spotter_storage(self) -> None:
        if self.old_tokens_file.exists():
            os.remove(self.old_tokens_file)
        if self.tokens_file.exists():
            os.remove(self.tokens_file)

    def print_spotter_storage(self) -> None:
        if self.tokens_file.exists():
            storage = self.tokens_file.read_text()
            print(f"  Spotter storage:\n{storage}")

    def run_action(self, action_id: str) -> None:
        # pylint: disable=too-many-branches,too-many-statements
        if action_id == "1":
            print(f"Action {action_id}: delete Spotter storage")
            self.delete_spotter_storage()
        elif action_id == "2":
            print(f"Action {action_id}: log in to site 1 using username, password")
            _, err = self.login(
                endpoint=self.endpoint1,
                username=self.username1,
                password=self.password1
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "3":
            print(f"Action {action_id}: log in to site 1 using token A1")
            _, err = self.login(
                endpoint=self.endpoint1,
                api_token=self.api_token1
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "4":
            print(f"Action {action_id}: scan at site 1")
            _, err = self.run_scan(
                endpoint=self.endpoint1,
                scan_input=self.scan_input
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "5":
            print(f"Action {action_id}: scan at site 1 using username, password")
            _, err = self.run_scan(
                endpoint=self.endpoint1,
                scan_input=self.scan_input,
                username=self.username1,
                password=self.password1
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "6":
            print(f"Action {action_id}: scan at site 1 using token A1")
            _, err = self.run_scan(
                endpoint=self.endpoint1,
                scan_input=self.scan_input,
                api_token=self.api_token1
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "7":
            print(f"Action {action_id}: create site 1 token B")
            self.api_token1_b_json = self.create_api_token_json(
                endpoint=self.endpoint1,
                api_token=self.api_token1
            )
            print(f"  Created token: {self.api_token1_b_json}")
        elif action_id == "8":
            print(f"Action {action_id}: log in to site 1 using token B")
            _, err = self.login(
                endpoint=self.endpoint1,
                api_token=self.api_token1_b_json["key"]
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "9":
            print(f"Action {action_id}: scan at site 1 using token B")
            _, err = self.run_scan(
                endpoint=self.endpoint1,
                scan_input=self.scan_input,
                api_token=self.api_token1_b_json["key"]
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "A":
            print(f"Action {action_id}: delete site 1 token B")
            self.delete_api_token(
                endpoint=self.endpoint1,
                api_token=self.api_token1,
                id_token_to_delete=self.api_token1_b_json["id"]
            )
        elif action_id == "B":
            print(f"Action {action_id}: log in to site 2 using username, password")
            _, err = self.login(
                endpoint=self.endpoint2,
                username=self.username2,
                password=self.password2
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "C":
            print(f"Action {action_id}: log in to site 2 using token A2")
            _, err = self.login(
                endpoint=self.endpoint2,
                api_token=self.api_token2
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "D":
            print(f"Action {action_id}: scan at site 2")
            _, err = self.run_scan(
                endpoint=self.endpoint2,
                scan_input=self.scan_input
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "E":
            print(f"Action {action_id}: scan at site 2 using username, password")
            _, err = self.run_scan(
                endpoint=self.endpoint2,
                scan_input=self.scan_input,
                username=self.username2,
                password=self.password2
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()
        elif action_id == "F":
            print(f"Action {action_id}: scan at site 2 using token A1")
            _, err = self.run_scan(
                endpoint=self.endpoint2,
                scan_input=self.scan_input,
                api_token=self.api_token2
            )
            print(f"  Standard error: {err}")
            self.print_spotter_storage()

    def run_sequence(self, sequence: str) -> None:
        print(f"Running sequence: {sequence}")
        print("::::::::::::::::::::::::")
        for action_code in sequence:
            self.run_action(action_code)
            print("-----------------------")

    def get_sequence(self, sequence_length: int) -> str:
        sequence: list[str] = ["1"]

        # Action pool of actions. These don"t require the second API token
        # to have been created.
        next_action_pool: list[str] = [
            "2", "3", "4", "5", "6", "7", "B", "C", "D", "E", "F"
        ]
        do_delete_api_token_b: bool = False
        while len(sequence) < sequence_length:
            next_action_id = random.choice(next_action_pool)
            sequence.append(next_action_id)

            if next_action_id == "7":
                # Action pool modification: don"t create another API token,
                # but run actions that involve this second API token.
                do_delete_api_token_b = True
                try:
                    next_action_pool.remove("7")
                except ValueError:
                    pass
                next_action_pool.extend(["8", "9", "A"])
            elif next_action_id == "A":
                do_delete_api_token_b = False

        if do_delete_api_token_b:
            # Finish the sequence with the second API token deletion as a cleanup.
            sequence.append("A")

        return "".join(sequence)


def parse_args() -> argparse.Namespace:
    final_parser = argparse.ArgumentParser(
        description="Random walk tester.", formatter_class=ArgumentDefaultsHelpFormatter, add_help=True
    )

    final_parser.add_argument(
        "--username1", help="Username of Spotter instance 1", type=str, required=True
    )
    final_parser.add_argument(
        "--password1", help="Password of Spotter instance 1", type=str, required=True
    )
    final_parser.add_argument(
        "--api-token1", help="API token of Spotter instance 1", type=str, required=True
    )
    final_parser.add_argument(
        "--endpoint1", help="Endpoint URL of Spotter instance 1", type=str, required=True
    )

    final_parser.add_argument(
        "--username2", help="Username of Spotter instance 2", type=str, required=True
    )
    final_parser.add_argument(
        "--password2", help="Password of Spotter instance 2", type=str, required=True
    )
    final_parser.add_argument(
        "--api-token2", help="API token of Spotter instance 2", type=str, required=True
    )
    final_parser.add_argument(
        "--endpoint2", help="Endpoint URL of Spotter instance 2", type=str, required=True
    )

    final_parser.add_argument(
        "--scan-input", help="Path to the scan input", type=str, required=True
    )

    return final_parser.parse_args(sys.argv[1:])


randomWalk = RandomWalk(parse_args())
randomWalk.run_sequence(randomWalk.get_sequence(23))
randomWalk.run_sequence("1B3479A96")
