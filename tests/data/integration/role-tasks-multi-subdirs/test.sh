#!/bin/bash

set -euxo pipefail

payload_file="/tmp/spotter-role-tasks-multi-subdirs.json"

spotter scan \
    --upload-values \
    --upload-metadata \
    --export-payload $payload_file \
    role/

function file_contains () {
    grep -q "$1" "$2"
}

file_contains "role/handlers/main.yml" "$payload_file"
file_contains "role/tasks/folder1/test.yml" "$payload_file"
file_contains "role/tasks/folder2/test1.yml" "$payload_file"
file_contains "role/tasks/folder2/test2.yml" "$payload_file"
file_contains "role/tasks/folder3/subfolder1/test1.yml" "$payload_file"
file_contains "role/tasks/folder3/subfolder1/test2.yml" "$payload_file"
file_contains "role/tasks/folder3/subfolder1/subsubfolder1/test.yml" "$payload_file"
file_contains "role/tasks/folder3/subfolder2/test.yml" "$payload_file"
file_contains "role/tasks/folder3/test.yml" "$payload_file"
file_contains "role/tasks/main.yml" "$payload_file"
file_contains "role/tasks/test.yml" "$payload_file"
