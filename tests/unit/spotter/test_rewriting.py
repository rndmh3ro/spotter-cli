from pathlib import Path
from typing import Any

import pytest
import ruamel.yaml as yaml

from spotter.parsing.parsing import SafeLineLoader
from spotter.rewriting.models import CheckType, RewriteSuggestion, RewriteResult
from spotter.rewriting.processor import RewriteProcessor, update_files
from spotter.rewriting.rewrite_action_inline import RewriteActionInline
from spotter.rewriting.rewrite_action_object import RewriteActionObject
from spotter.rewriting.rewrite_always_run import RewriteAlwaysRun
from spotter.rewriting.rewrite_fqcn import RewriteFqcn
from spotter.rewriting.rewrite_inline import RewriteInline
from spotter.rewriting.rewrite_local_action_inline import RewriteLocalActionInline
from spotter.rewriting.rewrite_local_object import RewriteLocalActionObject
from spotter.rewriting.rewrite_module_inline import RewriteModuleInline
from spotter.rewriting.rewrite_module_object import RewriteModuleObject


class TestRewriting:
    @pytest.fixture()
    def ansible_playbook_before_rewrite(self) -> list[dict[str, Any]]:
        return [
            {
                "name": "OpenSSL example",
                "hosts": "localhost",
                "tasks": [
                    {
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "openssl_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    }
                ],
            }
        ]

    @pytest.fixture()
    def ansible_playbook_after_rewrite(self) -> list[dict[str, Any]]:
        return [
            {
                "name": "OpenSSL example",
                "hosts": "localhost",
                "tasks": [
                    {
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "community.crypto.x509_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    }
                ],
            }
        ]

    def test_suggestion_from_task(self) -> None:
        task = {
            "task_id": "c9d23f56-03d5-4c11-a935-c78d33e22936",
            "task_args": {
                "name": "Ensure that the server certificate belongs to the specified private key",
                "openssl_certificate": {
                    "path": "{{ config_path }}/certificates/server.crt",
                    "privatekey_path": "{{ config_path }}/certificates/server.key",
                    "provider": "assertonly",
                },
            },
            "spotter_metadata": {
                "file": "/tmp/playbook.yaml",
                "line": 5,
                "column": 7,
                "start_mark_index": 64,
                "end_mark_index": 325,
            },
        }
        suggestion = {
            "data": {
                "after": "community.crypto.x509_certificate",
                "before": "openssl_certificate",
            },
            "action": "FIX_FQCN",
        }

        assert RewriteSuggestion.from_item(CheckType.TASK, task, suggestion) == RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={
                "name": "Ensure that the server certificate belongs to the specified private key",
                "openssl_certificate": {
                    "path": "{{ config_path }}/certificates/server.crt",
                    "privatekey_path": "{{ config_path }}/certificates/server.key",
                    "provider": "assertonly",
                },
            },
            file=Path("/tmp/playbook.yaml"),
            file_parent=Path("/tmp"),
            start_mark=64,
            end_mark=325,
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
        )

    def test_update_content(
            self,
            ansible_playbook_before_rewrite: list[dict[str, Any]],
            ansible_playbook_after_rewrite: list[dict[str, Any]],
    ) -> None:
        content = yaml.round_trip_dump(ansible_playbook_before_rewrite)
        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={
                "name": "Ensure that the server certificate belongs to the specified private key",
                "openssl_certificate": {
                    "path": "{{ config_path }}/certificates/server.crt",
                    "privatekey_path": "{{ config_path }}/certificates/server.key",
                    "provider": "assertonly",
                },
            },
            file=Path("/tmp/playbook.yml"),
            file_parent=Path("/tmp"),
            start_mark=64,
            end_mark=325,
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
        )

        assert RewriteProcessor.execute(content, suggestion) == RewriteResult(  # pylint: disable=protected-access
            content=yaml.round_trip_dump(ansible_playbook_after_rewrite),
            diff_size=14,
        )

    def test_update_files(
        self,
        tmp_path: Path,
        ansible_playbook_before_rewrite: list[dict[str, Any]],
        ansible_playbook_after_rewrite: list[dict[str, Any]],
    ) -> None:
        playbook_path = tmp_path / "playbook.yml"
        playbook_path.write_text(yaml.round_trip_dump(ansible_playbook_before_rewrite))

        update_files(
            [
                RewriteSuggestion(
                    check_type=CheckType.TASK,
                    item_args={
                        "name": "Ensure that the server certificate belongs to the specified private key",
                        "openssl_certificate": {
                            "path": "{{ config_path }}/certificates/server.crt",
                            "privatekey_path": "{{ config_path }}/certificates/server.key",
                            "provider": "assertonly",
                        },
                    },
                    file=playbook_path,
                    file_parent=tmp_path,
                    start_mark=64,
                    end_mark=325,
                    suggestion_spec={
                        "data": {"after": "community.crypto.x509_certificate", "before": "openssl_certificate"},
                        "action": "FIX_FQCN",
                    },
                )
            ]
        )

        with playbook_path.open("r", encoding="utf-8") as f:
            playbook_rewritten = yaml.round_trip_dump(yaml.safe_load(f))

        assert playbook_rewritten == yaml.round_trip_dump(ansible_playbook_after_rewrite)

    def test_fqcn_apply(self) -> None:
        with open("tests/data/rewriting/input/fqcn.yml", "r", encoding="utf-8") as f:
            content = f.read()
        with open("tests/data/rewriting/output/fqcn.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=6,
            end_mark=len(content),
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
        )

        rewriter = RewriteFqcn()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert rewrite_result.content == expected
        assert rewrite_result.diff_size == 14

    def test_fqcn_diff(self) -> None:
        with open("tests/data/rewriting/input/fqcn.yml", "r", encoding="utf-8") as f:
            content = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=6,
            end_mark=len(content),
            suggestion_spec={
                "data": {
                    "after": "community.crypto.x509_certificate",
                    "before": "openssl_certificate",
                },
                "action": "FIX_FQCN",
            },
        )

        rewriter = RewriteFqcn()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        diff_minus, diff_plus = replacement.get_diff()
        assert diff_minus == "    openssl_certificate:"
        assert diff_plus == "    community.crypto.x509_certificate:"

    def test_inline_apply(self) -> None:
        with open("tests/data/rewriting/input/inline.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/inline.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_INLINE",
                "data": {
                    "args": {"file": "my_test_tasks.yml"},
                    "vars": {"a": "b", "c": "d"},
                    "module_name": "include_tasks",
                },
            },
        )

        rewriter = RewriteInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert rewrite_result.diff_size > 0
        assert rewrite_result.content == expected

        with open("tests/data/rewriting/input/inline2.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/inline2.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_INLINE",
                "data": {
                    "args": {"file": "my_test_tasks.yml"},
                    "vars": {"a": "b", "c": "d"},
                    "module_name": "include_tasks",
                },
            },
        )

        rewriter = RewriteInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert rewrite_result.diff_size > 0
        assert rewrite_result.content == expected

    def test_always_run_apply(self) -> None:
        with open("tests/data/rewriting/input/always_run.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/always_run.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={"action": "FIX_ALWAYS_RUN", "data": {"module_name": "always_run"}},
        )

        rewriter = RewriteAlwaysRun()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_action_inlined(self) -> None:
        with open("tests/data/rewriting/input/action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/action_inlined.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_ACTION_INLINE",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                },
            },
        )

        rewriter = RewriteActionInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_local_action_object(self) -> None:
        with open("tests/data/rewriting/input/local_action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/local_action_object.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_LOCAL_ACTION_OBJECT",
                "data": {
                    "original_module_name": "local_action",
                    "module_name": "file",
                    "additional": [{"delegate_to": "localhost"}],
                },
            },
        )

        rewriter = RewriteLocalActionObject()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_local_action_inlined(self) -> None:
        with open("tests/data/rewriting/input/local_action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/local_action_inlined.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=len(content),
            suggestion_spec={
                "action": "FIX_LOCAL_ACTION_INLINE",
                "data": {
                    "original_module_name": "local_action",
                    "module_name": "file",
                    "additional": [{"delegate_to": "localhost"}],
                },
            },
        )

        rewriter = RewriteLocalActionInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_action_object(self) -> None:
        with open("tests/data/rewriting/input/action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/action_object.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_ACTION_OBJECT",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                    "additional": []
                },
            },
        )

        rewriter = RewriteActionObject()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_module_inline(self) -> None:
        with open("tests/data/rewriting/input/action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
        with open("tests/data/rewriting/output/module_inline.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=0,
            end_mark=len(content),
            suggestion_spec={
                "action": "FIX_ACTION_INLINE",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                },
            },
        )

        rewriter = RewriteModuleInline()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_module_object(self) -> None:
        with open("tests/data/rewriting/input/action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/module_object.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = RewriteSuggestion(
            check_type=CheckType.TASK,
            item_args={},
            file=Path(),
            file_parent=Path(),
            start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
            end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
            suggestion_spec={
                "action": "FIX_ACTION_OBJECT",
                "data": {
                    "original_module_name": "action",
                    "module_name": "file",
                },
            },
        )

        rewriter = RewriteModuleObject()
        replacement = rewriter.get_replacement(content, suggestion)
        assert replacement is not None

        rewrite_result = replacement.apply()
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_action_inlined(self) -> None:
        with open("tests/data/rewriting/input/action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_ACTION_INLINE",
                    "data": {
                        "original_module_name": "action",
                        "module_name": "file",
                        "additional": [{"delegate_to": "localhost"}],
                    },
                },
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_INLINE",
                    "data": {"args": {"mode": "0644", "state": "present"}, "module_name": "file"},
                },
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_local_action_inlined(self) -> None:
        with open("tests/data/rewriting/input/local_action_inlined.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_LOCAL_ACTION_INLINE",
                    "data": {
                        "original_module_name": "local_action",
                        "module_name": "file",
                        "additional": [{"delegate_to": "localhost"}],
                    },
                },
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_INLINE",
                    "data": {"args": {"mode": "0644", "state": "present"}, "module_name": "file"},
                },
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_action_object(self) -> None:
        with open("tests/data/rewriting/input/action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_ACTION_OBJECT",
                    "data": {
                        "original_module_name": "action",
                        "module_name": "file",
                        "additional": []
                    },
                },
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected

    def test_multiexecute_local_action_object(self) -> None:
        with open("tests/data/rewriting/input/local_action_object.yml", "r", encoding="utf-8") as f:
            content = f.read()
            f.seek(0)
            as_yml = yaml.load(f, SafeLineLoader, "1.1")
        with open("tests/data/rewriting/output/multi_execute.yml", "r", encoding="utf-8") as f:
            expected = f.read()

        suggestion = [
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "action": "FIX_LOCAL_ACTION_OBJECT",
                    "data": {
                        "original_module_name": "local_action",
                        "module_name": "file",
                        "additional": [{"delegate_to": "localhost"}],
                    },
                },
            ),
            RewriteSuggestion(
                check_type=CheckType.TASK,
                item_args={},
                file=Path(),
                file_parent=Path(),
                start_mark=as_yml[0]["__meta__"]["__start_mark_index__"],
                end_mark=as_yml[0]["__meta__"]["__end_mark_index__"],
                suggestion_spec={
                    "data": {
                        "after": "ansible.builtin.file",
                        "before": "file",
                    },
                    "action": "FIX_FQCN",
                },
            ),
        ]

        rewrite_result = RewriteProcessor.multi_execute(content, suggestion)
        assert abs(rewrite_result.diff_size) > 0
        assert rewrite_result.content == expected
