import argparse
from unittest import mock

import pytest
from pkg_resources import Distribution

from spotter.utils import get_current_cli_version, validate_url


class TestUtils:
    def test_get_current_cli_version(self) -> None:
        with mock.patch("pkg_resources.get_distribution", return_value=Distribution(version="2.2.2.dev12+gbd2cad1")):
            assert get_current_cli_version() == "2.2.2.dev12+gbd2cad1"

    @pytest.mark.parametrize("url", ["http://api.spotter.si/api", "https://api.spotter.si/api"])
    def test_validate_url(self, url: str) -> None:
        validate_url(url)

    def test_validate_url_invalid_scheme(self) -> None:
        url = "file://api.spotter.si/api"
        with pytest.raises(argparse.ArgumentTypeError) as exception_info:
            validate_url("file://api.spotter.si/api")

        assert str(exception_info.value) == f"URL '{url}' has an invalid URL scheme 'file', supported are: http, https."

    def test_validate_url_invalid_length(self) -> None:
        url = f"https://{'a' * 2048}"
        with pytest.raises(argparse.ArgumentTypeError) as exception_info:
            validate_url(url)

        assert str(exception_info.value) == f"URL '{url}' exceeds maximum length of 2048 characters."

    def test_validate_url_invalid_domain(self) -> None:
        url = "https://"
        with pytest.raises(argparse.ArgumentTypeError) as exception_info:
            validate_url(url)

        assert str(exception_info.value) == f"No URL domain specified in '{url}'."
