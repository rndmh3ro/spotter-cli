import json
from pathlib import Path
from typing import Callable, Optional
from unittest import mock

import pytest
import requests
from pkg_resources import Distribution

from spotter.storage import Storage

with mock.patch("pkg_resources.get_distribution", return_value=Distribution(version="2.2.2.dev12+gbd2cad1")):
    from spotter.api import ApiClient


class TestApi:
    TEST_API_TOKEN = "token"
    TEST_ACCESS_TOKEN = "access"
    TEST_REFRESH_TOKEN = "refresh"

    @pytest.fixture()
    def api_client(self, tmp_path: Path) -> ApiClient:
        api_client = ApiClient(
            "https://testapi.spotter.steampunk.si/api", Storage(tmp_path / "storage"), TestApi.TEST_API_TOKEN, "user",
            "hunter2"
        )
        return api_client

    @pytest.fixture()
    def mock_response(self) -> Callable[[int, str], requests.Response]:
        def _mock_response(status_code: int, content: str) -> requests.Response:
            response = requests.Response()
            response.status_code = status_code
            response._content = bytes(content, encoding="utf-8")  # pylint: disable=protected-access
            return response

        return _mock_response

    def test_url(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        assert api_client._url("/scans") == "https://testapi.spotter.steampunk.si/api/scans"

    def test_get_endpoint_tokens(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        endpoint_tokens = {
            "api_token": TestApi.TEST_API_TOKEN,
            "access": TestApi.TEST_ACCESS_TOKEN,
            "refresh": TestApi.TEST_REFRESH_TOKEN
        }
        api_client._storage.write_json(
            {"https://testapi.spotter.steampunk.si/api": endpoint_tokens}, api_client._storage_tokens_path
        )

        assert api_client._get_endpoint_tokens() == endpoint_tokens

    def test_get_api_token(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        api_client._storage.write_json({
            "https://testapi.spotter.steampunk.si/api": {
                "api_token": TestApi.TEST_API_TOKEN
            }
        }, api_client._storage_tokens_path)

        assert api_client._get_api_token() == TestApi.TEST_API_TOKEN

    def test_get_access_refresh_tokens(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        api_client._storage.write_json({
            "https://testapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }, api_client._storage_tokens_path)

        assert api_client._get_access_refresh_tokens() == (TestApi.TEST_ACCESS_TOKEN, TestApi.TEST_REFRESH_TOKEN)

    def test_old_tokens_fallback(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        api_client._storage_tokens_path = "tokens"
        api_client._storage.write_json({
            "access": TestApi.TEST_ACCESS_TOKEN,
            "refresh": TestApi.TEST_REFRESH_TOKEN
        }, api_client._storage_tokens_path)
        api_client._old_tokens_fallback()

        assert api_client._storage.read_json(api_client._storage_tokens_path) == {
            "https://testapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }

    def test_update_endpoint_tokens(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        api_client._storage.write_json({
            "https://testapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }, api_client._storage_tokens_path)
        api_client._update_endpoint_tokens({"api_token": TestApi.TEST_API_TOKEN})

        assert api_client._storage.read_json(api_client._storage_tokens_path) == {
            "https://testapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN,
                "api_token": TestApi.TEST_API_TOKEN
            }
        }

    def test_login(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response]) -> None:
        response_get = mock_response(200, "")
        response_post = mock_response(200, json.dumps({
            "api_token": TestApi.TEST_API_TOKEN
        }))
        # pylint: disable=protected-access
        assert api_client._storage.exists(api_client._storage_tokens_path) is False
        with mock.patch("requests.get", return_value=response_get), mock.patch("requests.post",
                                                                               return_value=response_post):
            api_client.login()

        assert api_client._storage.exists(api_client._storage_tokens_path) is True
        assert api_client._storage.read_json(api_client._storage_tokens_path) == {
            "https://testapi.spotter.steampunk.si/api": {
                "api_token": TestApi.TEST_API_TOKEN
            }
        }

    def test_refresh_login(self, api_client: ApiClient,
                           mock_response: Callable[[int, str], requests.Response]) -> None:
        # pylint: disable=protected-access
        api_client._storage.write_json({
            "https://testapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }, api_client._storage_tokens_path)

        response = mock_response(200, json.dumps({
            "access": "access-refreshed",
            "refresh": TestApi.TEST_REFRESH_TOKEN
        }))

        with mock.patch("requests.post", return_value=response):
            api_client._refresh_login()

        assert api_client._storage.read_json(api_client._storage_tokens_path) == {
            "https://testapi.spotter.steampunk.si/api": {
                "access": "access-refreshed",
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }

    def test_logout(self, api_client: ApiClient) -> None:
        # pylint: disable=protected-access
        tokens_path = api_client._storage.path / api_client._storage_tokens_path
        tokens_path.write_text(json.dumps({
            "https://testapi.spotter.steampunk.si/api": {
                "api_token": TestApi.TEST_API_TOKEN
            },
            "https://myapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            },
            "https://herapi.spotter.steampunk.si/api": {
                "api_token": TestApi.TEST_API_TOKEN,
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }))

        assert api_client._storage.exists(api_client._storage_tokens_path) is True

        api_client.logout()
        assert api_client._storage.read_json(api_client._storage_tokens_path) == {
            "https://myapi.spotter.steampunk.si/api": {
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            },
            "https://herapi.spotter.steampunk.si/api": {
                "api_token": TestApi.TEST_API_TOKEN,
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }

        with pytest.raises(SystemExit) as exception_info:
            api_client.logout()
        assert exception_info.value.code == 0

        api_client._base_url = "https://myapi.spotter.steampunk.si/api"

        api_client.logout()
        assert api_client._storage.read_json(api_client._storage_tokens_path) == {
            "https://herapi.spotter.steampunk.si/api": {
                "api_token": TestApi.TEST_API_TOKEN,
                "access": TestApi.TEST_ACCESS_TOKEN,
                "refresh": TestApi.TEST_REFRESH_TOKEN
            }
        }

        api_client._storage.remove(api_client._storage_tokens_path)

        with pytest.raises(SystemExit) as exception_info:
            api_client.logout()
        assert exception_info.value.code == 2

    def do_request(self, mock_response: Callable[[int, str], requests.Response], func: Callable[..., requests.Response],
                   path: str, authorize: bool, status_code: int, method: Optional[str] = None) -> None:
        response_get = mock_response(200, "")
        response_login = mock_response(200, json.dumps({
            "api_token": TestApi.TEST_API_TOKEN
        }))
        response_main = mock_response(status_code, json.dumps({
            "example": 42
        }))

        with mock.patch("requests.get", return_value=response_get), \
                mock.patch("requests.post", return_value=response_login), \
                mock.patch("requests.request", return_value=response_main):
            if status_code < 400:
                response = func(method, path, authorize) if method else func(path, authorize)
                assert response.status_code == status_code
            else:
                with pytest.raises(SystemExit) as exception_info:
                    # pylint: disable=expression-not-assigned
                    func(method, path, authorize) if method else func(path, authorize)
                assert exception_info.value.code == 2

    @pytest.mark.parametrize(("method", "path", "authorize", "status_code"), [
        ("GET", "/scans", True, 200),
        ("POST", "/login", False, 201),
        ("PATCH", "/update_name", True, 302),
        ("PUT", "/update", False, 404),
        ("DELETE", "/user", True, 500)
    ])
    def test_request(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response],
                     method: str,
                     path: str, authorize: bool, status_code: int) -> None:
        # pylint: disable=protected-access
        self.do_request(mock_response, api_client._request, path, authorize, status_code, method)

    def test_get(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response]) -> None:
        self.do_request(mock_response, api_client.get, "/scans", True, 200)

    def test_post(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response]) -> None:
        self.do_request(mock_response, api_client.post, "/login", False, 201)

    def test_patch(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response]) -> None:
        self.do_request(mock_response, api_client.patch, "/update_name", True, 302)

    def test_put(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response]) -> None:
        self.do_request(mock_response, api_client.put, "/update", True, 404)

    def test_delete(self, api_client: ApiClient, mock_response: Callable[[int, str], requests.Response]) -> None:
        self.do_request(mock_response, api_client.delete, "/user", True, 500)

    def test_format_api_error(self, api_client: ApiClient,
                              mock_response: Callable[[int, str], requests.Response]) -> None:
        response = mock_response(500, json.dumps({
            "example": 42
        }))

        assert api_client.format_api_error(response) == "API error: 500"  # pylint: disable=protected-access
