#!/usr/bin/env python
import setuptools


setuptools.setup(
    setup_requires=["setuptools_scm"],
    use_scm_version={"local_scheme": "node-and-date"}
)
