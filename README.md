# Steampunk Spotter Command-Line Interface (CLI)

[![PyPI](https://img.shields.io/pypi/v/steampunk-spotter)](https://pypi.org/project/steampunk-spotter/)

[Steampunk Spotter] provides an Ansible Playbook Scanning Tool that analyzes
and offers recommendations for your Ansible Playbooks.

The Steampunk Spotter CLI enables the use from the console with the ability
to scan Ansible content such as playbooks, roles, collections, or task files.

The following instructions explain how to get started with the Steampunk
Spotter CLI to scan Ansible content and get recommendations.

## Installation
Steampunk Spotter CLI requires Python 3 and is available as a
[steampunk-spotter] Python package.

```shell
$ pip install steampunk-spotter
```

We recommend installing the package into a clean Python virtual environment.

## Usage
After the CLI is installed, you can explore its commands and options by
running `spotter --help`.
The `--help/-h` optional argument is also available for every command.

### Limitations
The current release version of Steampunk Spotter contains the following
limitations that also apply to the CLI: 

- with the FREE subscription plan, you can perform up to 100 scans/month,
- with the INDIVIDUAL, TEAM, or ENTERPRISE subscription plan you can perform 
  an unlimited number of scans,
- for the ENTERPRISE subscription plan, contact us at steampunk@xlab.si to
  discuss your needs.

### Authentication
To use CLI, you have to supply your Steampunk Spotter user account credentials.
If you don't have an account, use `spotter register` command that will direct
you to the page where you can create one.

Steampunk Spotter supports two kinds of credentials: 1) API token (can be
generated in the user settings within the Spotter App), and 2) username and
password.

- Use `--api-token/-t` global optional argument to supply your token 
  credential.
  Alternatively, set the `SPOTTER_API_TOKEN` environment variable to contain 
  the API token.
- Use `--username/-u` and `--password/-p` global optional arguments to supply
  your username ad password. Alternatively, set the `SPOTTER_USERNAME` and
  `SPOTTER_PASSWORD` environment variables.
- You can run `spotter <options> login` to persist your credentials in the
  Steampunk Spotter CLI's local storage, where `<options>` stand for one
  of the approaches described above.

After that, you can start scanning right away.

### Scanning
The CLI `spotter scan` command is used for scanning Ansible
content (playbooks, roles, collections, or task files) and returning the
scan results.

#### Ansible content
The `scan` command will automatically detect the type of your Ansible content
and scan it.
Here are some examples of running scans:

```shell
# scan playbook
$ spotter scan path/to/playbook.yaml

# scan multiple files at once
$ spotter scan path/to/taskfile.yaml \
               path/to/playbook.yaml \
               path/to/role \
               path/to/collection

# scan any folder that contains Ansible content
$ spotter scan path/to/folder
```

#### Selecting the target project
*This part is only relevant for users with a TEAM plan or higher.*

By default, the scan results are stored in the first project of the user's
first organization (in the app).

Users that have multiple organizations or projects in the app can use
`--project-id` optional argument to specify the UUID of an existing target
project, where the scan result will be stored.

```shell
$ spotter scan --project-id <project-id> .
```

You can learn your project id by logging into the app, selecting the
appropriate organization and navigating to the project's dashboard.

#### Including values
By default, CLI parses Ansible YAML content without any parameter values.
With values, we can discover additional tips for improvements, so if you want
to parse and send the values, you can use `--include-values` optional argument.

```shell
$ spotter scan --include-values playbook.yaml
```

#### Including metadata
By default, CLI collects and uses metadata (file names, line, and column
numbers, YAML markers) from Ansible content just for displaying the scan
output, which means that no data about your Ansible content structure is sent
to the backend server. 
For enriched user experience in the app and to get additional tips for
improvements, you can use `--include-metadata` optional argument to send the
metadata.

```shell
$ spotter scan --include-metadata playbook.yaml
```

#### Automated application of suggestions to your code
There is also a `--rewrite` optional argument that rewrites your files with
fixes after scanning.
This action will modify your files.

```shell
$ spotter scan --rewrite playbook.yaml
```

### Next steps
For more comprehensive usage, issue `spotter scan --help`.
Please refer to [DOCUMENTATION.md] for further instructions.

## Acknowledgment
This tool was created by [XLAB Steampunk], IT automation specialists and
leading experts in building Enterprise Ansible Collections.

[Steampunk Spotter]: https://spotter.steampunk.si/
[steampunk-spotter]: https://pypi.org/project/steampunk-spotter/
[DOCUMENTATION.md]: https://gitlab.com/xlab-steampunk/steampunk-spotter-client/spotter-cli/-/blob/main/DOCUMENTATION.md
[XLAB Steampunk]: https://steampunk.si/
